<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQueryCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('query_campaign', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('query_id');
            $table->foreign('query_id')->references('id')->on('queries');
            $table->unsignedBigInteger('campaign_id');
            $table->string('campaign_name');
            $table->longText('campaign_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('query_campaign');
    }
}
