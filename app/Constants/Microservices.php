<?php

namespace App\Constants;

use App\Http\RequestHandlers\MicroserviceARequestHandler;
use App\Http\RequestHandlers\MicroserviceBRequestHandler;
use App\Http\RequestHandlers\MicroserviceCRequestHandler;

class Microservices
{
    const MS_IDENTIFIER_A = 'ms_a';
    const MS_IDENTIFIER_B = 'ms_b';
    const MS_IDENTIFIER_C = 'ms_c';

    const MS_REQUEST_HANDLER_A = MicroserviceARequestHandler::class;
    const MS_REQUEST_HANDLER_B = MicroserviceBRequestHandler::class;
    const MS_REQUEST_HANDLER_C = MicroserviceCRequestHandler::class;
}