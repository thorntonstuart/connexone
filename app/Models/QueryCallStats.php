<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueryCallStats extends Model
{
    protected $guarded = [];

    public function parentQuery()
    {
        return $this->belongsTo(Query::class);
    }
}
