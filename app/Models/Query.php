<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Query extends Model
{
    protected $guarded = [];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function callStats(): HasOne
    {
        return $this->hasOne(QueryCallStats::class);
    }

    public function campaign(): HasOne
    {
        return $this->hasOne(QueryCampaign::class);
    }
}
