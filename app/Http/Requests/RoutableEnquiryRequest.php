<?php

namespace App\Http\Requests;

use App\Constants\Microservices;
use Illuminate\Foundation\Http\FormRequest;

class RoutableEnquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $permittedServices = collect();

        if ($this->checkMicroserviceCriteria(Microservices::MS_IDENTIFIER_A)) {
            $permittedServices->push(Microservices::MS_IDENTIFIER_A);
        }

        if ($this->checkMicroserviceCriteria(Microservices::MS_IDENTIFIER_B)) {
            $permittedServices->push(Microservices::MS_IDENTIFIER_B);
        }

        if ($this->checkMicroserviceCriteria(Microservices::MS_IDENTIFIER_C)) {
            $permittedServices->push(Microservices::MS_IDENTIFIER_C);
        }

        $this->merge([
            'services' => $permittedServices->toArray(),
        ]);
        
        return !empty($this->input('services'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];
    }

    public function data()
    {
        return $this->except(['services']);
    }

    /**
     * Customer query data only
     *
     * @return array
     */
    public function getCustomerQueryData(): array
    {
        return [
            'full_name' => $this->input('name'),
            'email' => $this->input('email'),
            'phone' => $this->input('phone'),
        ];
    }

    /**
     * Call stats data only
     *
     * @return array
     */
    public function getCallStatsData(): array
    {
        return [
            'call_stats_id' => $this->input('call_stats.id'),
            'call_length' => $this->input('call_stats.length'),
            'recording_url' => $this->input('call_stats.recording_url'),
        ];
    }

    /**
     * Campaign data only
     *
     * @return array
     */
    public function getCampaignData(): array
    {
        return [
            'campaign_id' => $this->input('campaign.id'),
            'campaign_name' => $this->input('campaign.name'),
            'campaign_description' => $this->input('campaign.description'),
        ];
    }

    /**
     * Check if request should be forwarded to microservice
     *
     * @param string $microserviceIdentifier
     * @return boolean
     */
    protected function checkMicroserviceCriteria(string $microserviceIdentifier): bool
    {
        if ($microserviceIdentifier === Microservices::MS_IDENTIFIER_A) {
            return $this->input('campaign.name') !== 'Campaign B';
        }

        if ($microserviceIdentifier === Microservices::MS_IDENTIFIER_B) {
            return $this->input('query_type.title') !== 'SALE MADE';
        }

        return true;
    }
}
