<?php

namespace App\Http\Controllers\Api;

use App\Constants\Microservices;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoutableEnquiryRequest;
use App\Http\RequestHandlers\MicroserviceARequestHandler;
use App\Http\RequestHandlers\MicroserviceBRequestHandler;
use App\Http\RequestHandlers\MicroserviceCRequestHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EnquiryController extends Controller
{
    /**
     * Collect responses from each service
     *
     * @var array
     */
    protected $serviceResponses;

    public function __construct()
    {
        $this->serviceResponses = [
            Microservices::MS_IDENTIFIER_A => null,
            Microservices::MS_IDENTIFIER_B => null,
            Microservices::MS_IDENTIFIER_C => null,
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoutableEnquiryRequest $request)
    {
        if (in_array(Microservices::MS_IDENTIFIER_A, $request->input('services'))) {
            $service = new MicroserviceARequestHandler($request);
            $this->serviceResponses[Microservices::MS_IDENTIFIER_A] = $service();
        }

        if (in_array(Microservices::MS_IDENTIFIER_B, $request->input('services'))) {
            $service = new MicroserviceBRequestHandler($request);
            $this->serviceResponses[Microservices::MS_IDENTIFIER_B] = $service();
        }

        if (in_array(Microservices::MS_IDENTIFIER_C, $request->input('services'))) {
            $service = new MicroserviceCRequestHandler($request);
            $this->serviceResponses[Microservices::MS_IDENTIFIER_C] = $service();
        }

        return response()->json(Response::HTTP_OK, [
            'service_responses' => $this->serviceResponses,
        ]);
    }

    protected function runAllocatedService(string $classname)
    {
        $service = new $classname;
        return $service();
    }
}
