<?php

namespace App\Http\RequestHandlers;

use App\Http\Requests\RoutableEnquiryRequest;
use App\Http\RequestHandlers\AbstractMicroserviceRequestHandler;

class MicroserviceBRequestHandler extends AbstractMicroserviceRequestHandler
{
    /**
     * @var string
     */
    protected $endpoint;

    public function __construct(RoutableEnquiryRequest $request)
    {
        parent::__construct($request);

        $this->endpoint = config('microservices.ms_b.endpoint');
    }
}