<?php

namespace App\Http\RequestHandlers;

use App\Http\Requests\RoutableEnquiryRequest;
use App\Models\Customer;
use GuzzleHttp\Client;

abstract class AbstractMicroserviceRequestHandler
{
    /**
     * @var RoutableEnquiryRequest
     */
    protected $request;

    public function __construct(RoutableEnquiryRequest $request)
    {
        $this->request = $request;
    }

    public function __invoke()
    {
        $customer = $this->store();
        $client = $this->createClient();
        $response = $client->post('/', $this->request->data());

        return response()->json([
            'response' => $response->getBody()->getContents(),
        ]);
    }

    protected function store()
    {
        $customer = Customer::firstOrCreate([
            'email' => $this->request->input('email'),
        ]);

        $customer->query()->create($this->request->getCustomerQueryData());
        $customer->query()->callStats()->create($this->request->getCallStatsData());
        $customer->query()->campaign()->create($this->request->getCampaignData());

        return $customer;
    }
    
    protected function createClient()
    {
        return new Client([
            'base_uri' => $this->endpoint,
        ]);
    }
}